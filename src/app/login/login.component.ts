import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /**Class variables: only create when using it in html also */
  email: string = "";
  password: string = "";
  isWrong: boolean=false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  submit() {
    if (this.password.length < 5) {
      this.isWrong=true;

    } else {
      this.isWrong=false;
      alert(this.email + "correct login");
      this.router.navigate(['/home']);
    }

  }


}



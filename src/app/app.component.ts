import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'gaurav';
  sum: number;
  addNumber(a:number, b:number) {
    return  a + b;
  }

  constructor() {
    console.log("Called.....");
    this.sum = this.addNumber(10, 2);
    console.log(this.addNumber(10, 2));
  }
}
